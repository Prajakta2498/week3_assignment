module.exports = execFile => {
    execFile.on("exec",(msg1,msg2,msg3)=>{
        const cp = require('child_process');
        var students = msg3['data']
        
        console.log(students)

        var child = cp.fork("./file.js",students)

        child.on("message",(data)=>{
            console.log(`Welcome to Hogwarts ${data}`)
        })
        .on("exit",()=>{
            console.log("Child Terminated!!!!")
        })
    })
}